//Retrieve an element from the webpage
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");

txtFirstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
});

txtLastName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value;
});

/* 
ALTERNATIVE SOLUTION
const updateFullName = ()=>{
  let firstName = txtFirstName.value
  let lastName = txtLastName.value
  spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener('keyup', updateFullName)
txtLastName.addEventListener('keyup', updateFullName)
*/
