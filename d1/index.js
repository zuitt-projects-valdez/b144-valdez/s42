//Retrieve an element from the webpage
const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");
const txtLastName = document.querySelector("#txt-last-name");
/* 
  document - refers to the whole webpage or html file 
  querySelector - used to select a specific object (HTML element) from the document; retrieving the id with the querySelectorr

  ** alternatively can use the getElement functions to retrieve elements, but querySelectors are more flexible 
  - document.getElementById('id-name')
  - document.getElementByClassName('class-name')
  - document.getElementByTagName('tag')
*/

console.log(document); //the whole webpage will be selected

//performs an action when an event triggers
txtFirstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtFirstName.value;
  // console.log(event.target);
  // console.log(event.target.value);
});
txtFirstName.addEventListener("keyup", (event) => {
  //spanFullName.innerHTML = txtFirstName.value;

  //event.target contains the element WHERE the event happened
  console.log(event.target);
  //event.target.value gets the VALUE of the input object
  console.log(event.target.value);
});

/* 
  addEventListener('event', 'function to be executed')
  .innerHTML - based on the value that is targeted (txtFirstName), the info will be given/transferred to the object attached (spanFullName)
*/

//Mini activity
txtLastName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = txtLastName.value;
  console.log(event.target);
  console.log(event.target.value);
});

//^ the two events will overwrite each other rather than combining

//Activity
